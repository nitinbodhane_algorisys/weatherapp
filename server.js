var express = require('express');
var request = require('request');
var cfenv = require('cfenv');
var helmet = require('helmet');

//setup middleware
var app = express();
var ninetyDaysInMilliseconds = 7776000000;

  app.use(express.static(__dirname + '/public'));
  // set the HTTP Strict Transport Security (HSTS) header for 90 days
  app.use(helmet.hsts({
	  maxAge: ninetyDaysInMilliseconds,
	  includeSubdomains: true,
	  force: true
  }));
  // Prevent Cross-site scripting (XSS) attacks
  app.use(helmet.xssFilter());


var appEnv = cfenv.getAppEnv();
// console.log(appEnv);
var weather_host = appEnv.services["weatherinsights"]
        ? appEnv.services["weatherinsights"][0].credentials.url // Weather credentials passed in
        : "https://5e4bb446-0156-4831-9a73-37bfdb5b5281:Kcr6GRAceh@twcservice.au-syd.mybluemix.net"; // or copy your credentials url here for standalone

function weatherAPI(path, qs, done) {
    var url = weather_host + path;
    console.log(url, qs);
    request({
        url: url,
        method: "GET",
        headers: {
            "Content-Type": "application/json;charset=utf-8",
            "Accept": "application/json"
        },
        qs: qs
    }, function(err, req, data) {
        if (err) {
            done(err);
        } else {
            if (req.statusCode >= 200 && req.statusCode < 400) {
                try {
                    done(null, JSON.parse(data));
                } catch(e) {
                    console.log(e);
                    done(e);
                }
            } else {
                console.log(err);
                done({ message: req.statusCode, data: data });
            }
        }
    });
}

app.get('/api/forecast/dailyData', function(req, res) {
    // console.log("In daily data service")
    var geocode = (req.query.geocode || "18.97,72.83").split(",");
    weatherAPI("/api/weather/v1/geocode/" + geocode[0] + "/" + geocode[1] + "/forecast/daily/3day.json",{}, function(err, result) {
        if (err) {
            console.log(err);
            res.send(err).status(400);
        } else {
            console.log("3 days Forecast");
            res.json(result);
        }
    });
});

app.listen(appEnv.port, appEnv.bind, function() {
  console.log("server starting on " + appEnv.url);
});

