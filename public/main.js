
// callback - done(err, result)
function weatherAPI(path, qs, done) {
	
   	$.ajax({
		url: path,
		type: 'GET',
		contentType:'application/json',
		data: qs,
  		success: function(data) {
  			if (data.message == 401) {
  				try {
  					data.data = JSON.parse(data.data);
  				} catch(e) {
  				}
				done(data);
  			} else {
  				done(null, data);
  			}
		},
		error: function(xhr, textStatus, thrownError) {
			done(textStatus);
		}
	});
}


function getWeatherData(geocode){
	
	weatherAPI("/api/forecast/dailyData", { 
		geocode: geocode
	}, function(err, data) {
  		if (err) {
  			console.log(err);
  		} else {
  			console.log(data);
  			displayDaily(data);
  		}
	});
}

function displayDaily(result) {
	
	var ul = document.getElementById("vertex")
	var li = document.createElement('li');

	var html = '';
	html += '<p> Lat-'+result.metadata.latitude+'  Long-'+result.metadata.latitude+'</p>';
	html += '<table>';
	html += '<tr>';
	html += '<th>Time</th>';
	html += '<th>Narrative</th>';
	html += '<th>Min Tempreature</th>';
	html += '<th>Max Tempreature</th>';
	html += '</tr>';
	for(var i = 0; i<result.forecasts.length;i++){
		html += '<tr><td>'+result.forecasts[i].dow+'</td><td>'+result.forecasts[i].narrative+'</td><td>'+result.forecasts[i].min_temp+'</td><td>'+result.forecasts[i].max_temp+'</td></tr>';
	}
	
	html += '</table>';

	li.innerHTML = html;

    ul.appendChild(li);
	console.log(result);
}


function init() {
	getLocation(function(err, geocode) {
		if (err) {
			setLocation();
		} else {
			addGeocode('Local', geocode);
			setLocation(geocode, "m", "en");
		}
	});
}

